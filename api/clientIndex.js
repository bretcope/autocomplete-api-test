var clientIndex = (function (module)
{
	if (!module || typeof module.exports !== 'object')
	{
		var module = { exports: {} };
	}
	
	/* ========================================================================================================
	 * Private Members Declaration (no methods)
	 * ===================================================================================================== */
	
	var _clientIndex = null;
	var _clientsById = null;

	/* ========================================================================================================
	 * Public Members Declaration (no methods)
	 * ===================================================================================================== */
	
	Object.defineProperty(module.exports, 'isReady', { get: function () { return _clientIndex !== null; } });

	/* ========================================================================================================
	 * Public Methods - Keep in alphabetical order
	 * ===================================================================================================== */
	
 	module.exports.buildIndex = function (clients)
	{
		_clientIndex = [];
		_clientsById = {};

		var parts;
		for (var i = 0; i < clients.length; i++)
		{
			_clientsById[clients[i].Id] = clients[i];
			
			parts = clients[i].ClientName.split(/\W+/g);
			for (var x in parts)
			{
				_clientIndex.push({ client: clients[i], token: parts[x].toLowerCase(), term: x });
			}
		}

		_clientIndex.sort(function (a, b)
		{
			if (a.token == b.token)
				return b.term - a.term;
			
			return a.token < b.token ? -1 : 1;
		});
	};
	
	module.exports.search = function (query, suggestionLimit)
	{
		var start = process.hrtime();

		var suggestions = [];
		if (!query)
			return suggestions;
		
		query = String(query).toLowerCase();
		var index, i;
		if (query.length === 1) // special case to improve the performance of single character query 
		{
			index = binarySearch(query, prefixCompare, true);
			
			if (index === -1)
				return suggestions;
			
			var firstPosition = true;
			i = index;
			while (true)
			{
				if (!firstPosition || _clientIndex[i].term == 0)
				{
					suggestions.push(toAutocompleteObject(_clientIndex[i].client));
				}
				
				if (suggestions.length >= suggestionLimit)
					break;
				
				i++;
				if (_clientIndex[i].token[0] !== query)
				{
					if (firstPosition)
					{
						firstPosition = false;
						i = index;
					}
					else
					{
						break;
					}
				}
			}
		}
		else
		{
			var parts = query.split(/\W+/g);
			
			var comparer, r;
			var ranges = [];
			for (i = 0; i < parts.length; i++)
			{
				if (i === parts.length - 1)
					comparer = prefixCompare;
				else
					comparer = fullCompare;
				
				r = binarySearch(parts[i], comparer);
				
				if (r === null)
					return suggestions;
				
				ranges.push(r);
			}
			
			var client, idHash;
			var lastHash = null;
			for (i = ranges.length - 1; i > -1; i--)
			{
				idHash = {};
				for (var x = ranges[i].start; x < ranges[i].end; x++)
				{
					client = _clientIndex[x].client;
					if (!idHash[client.Id] && (!lastHash || lastHash[client.Id]))
					{
						idHash[client.Id] = true;
						
						if (i === 0)
						{
							suggestions.push(toAutocompleteObject(client));
							
							if (suggestionLimit && suggestions.length >= suggestionLimit)
								break;
						}
					}
				}
				
				lastHash = idHash;
			}
		}
		
		suggestions.sort(suggestionSorter);
		var end = process.hrtime(start);
		end[1] /= 1000000;
		console.log(end);
		
		return suggestions;
	};
	
	module.exports.search2 = function (query, suggestionLimit)
	{
		console.log('=============================================');
		console.log('query: ' + query);

		startTimer('search total');
		var suggestions = [];
		if (!query)
			return suggestions;

		query = String(query).toLowerCase();
		var parts = query.split(/\W+/g);

		var comparer, i;
		var results = null;
		for (i = 0; i < parts.length; i++)
		{
			if (!parts[i])
				continue;
			
			if (i === parts.length - 1)
				comparer = prefixCompare;
			else
				comparer = fullCompare;

			results = binarySearch2(parts[i], comparer, i, results);

			if (results === null)
				return suggestions;
		}
		
		if (results === null)
			return suggestions;
		
		startTimer('buckets');
		var buckets = {};
		var highest = 0;
		for (i in results)
		{
			if (buckets[results[i]] === undefined)
			{
				buckets[results[i]] = [];
				if (results[i] > highest)
					highest = results[i];
			}
			
			buckets[results[i]].push(toAutocompleteObject(_clientsById[i]));
		}
//		endTimer('buckets');
		
		startTimer('concat');
		if (buckets[0])
			suggestions = buckets[0];
		
		highest++;
		for (i = 1; i < highest && suggestions.length < suggestionLimit; i++)
		{
			if (buckets[i])
				suggestions = suggestions.concat(buckets[i]);
		}
//		endTimer('concat');

		startTimer('sort2');
		suggestions.sort(suggestionSorter);
//		endTimer('sort2');
		endTimer('search total');
//		console.log();

		return suggestions;
	};

	/* ========================================================================================================
	 * Private Methods - Keep in alphabetical order
	 * ===================================================================================================== */
	
	function binarySearch (token, comparer, firstOnly)
	{
		var lower = 0;
		var upper = _clientIndex.length - 1;
		var i, comp;

		while (true)
		{
			i = Math.floor((upper - lower) / 2) + lower;

			comp = comparer(token, _clientIndex[i].token);
			if (comp === 0)
				break;

			if (lower === upper)
				return null;

			if (comp === -1)
				upper = i - 1;
			else
				lower = i + 1;
		}
		
		// find other matching indexes
		var range = { start: i, end: i + 1 };
		
		while (range.start > 0 && comparer(token, _clientIndex[range.start - 1].token) === 0)
		{
			range.start--;
		}
		
		if (firstOnly)
			return range.start;
		
		while (range.end < _clientIndex.length && comparer(token, _clientIndex[range.end].token) === 0)
		{
			range.end++;
		}
		
		return range;
	}
	
	function binarySearch2 (token, comparer, term, resultsFilter)
	{
		startTimer('binary2');
		var results = {};
		var lower = 0;
		var upper = _clientIndex.length - 1;
		var i, comp;
		
		function addIndex(dex)
		{
			var item = _clientIndex[dex];
			
			if (resultsFilter && resultsFilter[item.client.Id] === undefined)
				return;
			
			var diff = Math.abs(term - item.term);
			
			if (results[item.client.Id] === undefined || results[item.client.Id] > diff)
			{
				results[item.client.Id] = resultsFilter ? Math.max(diff, resultsFilter[item.client.Id]) : diff;
			}
		}

		while (true)
		{
			i = Math.floor((upper - lower) / 2) + lower;

			comp = comparer(token, _clientIndex[i].token);
			if (comp === 0)
				break;

			if (lower === upper)
				return null;

			if (comp === -1)
				upper = i - 1;
			else
				lower = i + 1;
		}
		
		addIndex(i);
		var start = i;

		while (i > 0 && comparer(token, _clientIndex[i - 1].token) === 0)
		{
			i--;
			addIndex(i);
		}
		
		i = start + 1;
		while (i < _clientIndex.length && comparer(token, _clientIndex[i].token) === 0)
		{
			addIndex(i);
			i++;
		}
		
//		endTimer('binary2');
		return results;
	}
		
	function fullCompare (a, b)
	{
		if (a == b)
			return 0;
		
		return a < b ? -1 : 1;
	}
	
	function prefixCompare (prefix, token)
	{
		token = token.substr(0, prefix.length);
		if (token == prefix)
			return 0;

		return prefix < token ? -1 : 1;
	}
	
	function suggestionSorter (a, b)
	{
		if (a.label == b.label)
			return 0;
		
		return a.label < b.label ? -1 : 1;
	}
	
	function toAutocompleteObject (client)
	{
//		return { label: client.ClientName, value: client.Id };
		return { text: client.ClientName, id: client.Id };
	}
	
	/* ========================================================================================================
	 * Initialization
	 * ===================================================================================================== */
	
	var _timers = {};
	
	function startTimer (name)
	{
		_timers[name] = process.hrtime();
	}
	
	function endTimer (name)
	{
		var end = process.hrtime(_timers[name]);
		end[1] /= 1000000;
		
		var output = name + ': ';
		if (end[0] > 0)
			output += end[0] + ' seconds';
		
		output += end[1].toFixed(3) + ' ms';
		console.log(output);
	}
	
	return module.exports;
	
})(module);