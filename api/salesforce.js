var fs = require('fs');
var path = require('path');
var clientIndex = require('./clientIndex.js');

var _clients = null;

module.exports.autocomplete = function (request, response)
{
	// for CORS
	var origin;
	if (origin = request.get('origin'))
	{
		response.set('Access-Control-Allow-Origin', origin);
	}
	
	var handler = autocompleteHandler.bind(this, request, response);
	
	if (!clientIndex.isReady)
		updateAllClients(handler);
	else
		handler();
};

function autocompleteHandler (request, response, error)
{
	if (error)
	{
		var json = { error: error instanceof Error ? error.message : error };
		response.json(500, json);
		return;
	}

	var suggestions = clientIndex.search2(request.params.query, 10);
	if (suggestions.length > 10)
		suggestions = suggestions.slice(0, 10);
	
	response.json(suggestions);
}

function updateAllClients (callback)
{
	//this would actually be a db call...
	
	fs.readFile(path.join(__dirname, 'sample.json'), { encoding: 'utf8' }, function (error, json)
	{
		if (error)
		{
			callback(error);
			return;
		}
		
		try
		{
			_clients = JSON.parse(json);
			clientIndex.buildIndex(_clients);
			callback();
		}
		catch (ex)
		{
			callback(ex);
		}
	});
}