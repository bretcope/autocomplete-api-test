$(function ()
{
	function getSuggestions (query, callback)
	{
		callback = query.callback;
		$.ajax({ url: '/api/salesforce/autocomplete/' + encodeURIComponent(query.term) })
			.done(function (data, textStatus, jqXHR)
			{
				callback({ results: data });
			})
			.fail(function (jqXHR, textStatus, errorThrown)
			{
				console.log(errorThrown);
				callback({ results: [] });
			});
	}
	
//	$('.salesforce-client-input').autocomplete({ source: getSuggestions });
	$('.salesforce-client-input').select2({ query: getSuggestions, width: '400px' });
});