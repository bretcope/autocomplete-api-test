var express = require('express');
var app = express();

var api = {};
api.salesforce = require('./api/salesforce.js');

app.use('/static', express.static('static'));

app.get('/', function (request, response)
{
	response.sendfile('index.html');
});

app.get('/api/salesforce/autocomplete/:query', api.salesforce.autocomplete);

app.listen(3023, function () { console.log("listening on 3023..."); });